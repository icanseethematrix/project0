<!-- Course: CS164 Project0
     Author: Rishav Mukherji 
	 Function: Parses courses.xml into a database.
-->
<?php
    //for connection of database
    $con=mysql_connect("localhost", "jharvard","crimson");
    mysql_select_db("jharvard_project0", $con);

    $source = 'courses.xml';
    
	// load as file
    $harvard_courses = simplexml_load_file($source);
        
    // going over the XML file
    foreach($harvard_courses->course as $course) 
    {
        $cat_num = $course['cat_num'];      
        mysql_query("INSERT INTO course (cat_num) VALUES ('$cat_num')");
        
        $spring_term = $course->term['spring_term'];
        // if($spring_term == "Y")
        // {
            $course_group_code = $course->course_group['code'];
            // mysql_query("INSERT INTO course (course_group_code) VALUES ('$course_group_code')");
            // mysql_query("INSERT INTO course_group (course_group_code) VALUES ('$course_group_code')");

            $course_group_name = $course->course_group;
            // mysql_query("INSERT INTO course_group (course_group_name) VALUES ('$course_group_name')");

            $course_number = $course->course_number->num_int;
            // mysql_query("INSERT INTO course (course_number) VALUES ($course_number)");

            $title = $course->title;
            // mysql_query("INSERT INTO course (title) VALUES ('$title')");

            $description = $course->description;
            // mysql_query("INSERT INTO course (description) VALUES ('$description')");

            $faculty_id = $course->faculty_list->faculty['id'];
            // mysql_query("INSERT INTO course (faculty_id) VALUES ('$faculty_id')");
            // mysql_query("INSERT INTO faculty (faculty_id) VALUES ('$faculty_id')");

            /*$faculty_name = ($course->faculty_list->faculty->name->prefix).
                            ($course->faculty_list->faculty->name->first).
                            ($course->faculty_list->faculty->name->middle).
                            ($course->faculty_list->faculty->name->last).
                            ($course->faculty_list->faculty->name->suffix); 
            mysql_query("INSERT INTO faculty (faculty_name) VALUES ('$faculty_name')"); */

            $meeting_days="";
            $meeting_begin_time = ""; $meeting_end_time = ""; $meeting_location = "";
            foreach($course->meeting as $meetings)
            {
                $meeting_day = $course->meeting['day'];
                $meeting_days = $meeting_days.$meeting_day;

                $meeting_begin_time = $course->meeting['begin_time'];
                // mysql_query("INSERT INTO course (meeting_begin_time) VALUES ('$meeting_begin_time')");

                $meeting_end_time = $course->meeting['end_time'];
                // mysql_query("INSERT INTO course (meeting_end_time) VALUES ('$end')");

                $meeting_location = ($course->meeting_location->location['building']).
                                    ($course->meeting_location->location['room']);
                // mysql_query("INSERT INTO course (meeting_location) VALUES ('$meeting_location')");
            }
            // mysql_query("INSERT INTO course (meeting_day) VALUES ('$meeting_days')");

            $requirement_type = $course->requirements->requirement['type'];
            // mysql_query("INSERT INTO course (requirement_type) VALUES ('$requirement_type')");

            $requirement_name = $course->requirements->requirement['name'];
            // mysql_query("INSERT INTO course (requirement_name) VALUES ('$requirement_name')");
        // }
        mysql_query("INSERT INTO course VALUES ('$cat_num', '$course_group_code', $course_number, '$title',
                    '$description', '$faculty_id', '$meeting_days', '$meeting_begin_time', '$meeting_end_time',
                    '$meeting_location', '$requirement_type', '$requirement_name')");
        mysql_query("INSERT INTO course_group VALUES('$course_group_code', '$course_group_name'");
        mysql_query("INSERT INTO faculty VALUES('$faculty_id', '$faculty_name'");
    }
    echo "Done";
?>
