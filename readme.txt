# clone repo into ~/vhosts/icanseethematrix
cd ~/vhosts
git clone git@bitbucket.org:icanseethematrix/project0.git icanseethematrix

# chmod all directories 711
find ~/vhosts/icanseethematrix -type d -exec chmod 711 {} \;

# chmod all PHP files 600
find ~/vhosts/icanseethematrix -type f -name *.php -exec chmod 600 {} \;

# chmod most everything else 644
find ~/vhosts/icanseethematrix -type f \( -name *.css -o -name *.gif -o -name *.html -o -name *.js -o -name *.jpg -o -name *.png -o -name .htaccess \) -exec chmod 644 {} \;

# create a MySQL database for project
mysql -u jharvard -p -e 'CREATE DATABASE jharvard_icanseethematrix'

# append '127.0.0.1 icanseethematrix' to /etc/hosts
sudo gedit /etc/hosts
