/* 	index.php
	Muhammad Ibrahim
    Provides the basic UI for the mobile app */

<!DOCTYPE html> 
<html> 
	<head> 
        <title>Harvard Courses</title> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        
        <!-- Linkking stylesheets from external libraries -->
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
        <link rel="stylesheet" href="http://dev.jtsage.com/cdn/datebox/latest/jquery.mobile.datebox.css" />
        
        <!-- Adds javascript libraries like jquery -->
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
        <script src="http://dev.jtsage.com/cdn/datebox/latest/jquery.mobile.datebox.js"></script>	
	</head> 

<body> 

<!-- Pages inside the application-->
<div data-role="page" data-theme="a" id = "browse">	
    
    <div data-role="header">
		<div data-role="fieldcontain">
    		<input type="search" name="password" id="search" value="" /> <!-- Search Bar-->
		</div>
	</div>
    
    <div data-role="content">	
		<ul data-role="listview" data-theme="g">
			<li><a href=>Field 1</a></li>
			<li><a href=>Field 2</a></li>
			<li><a href=>Field 3</a></li>
            <li><a href=>Field 4</a></li>
            <li><a href=>Field 5</a></li>
            <li><a href=>Field 5</a></li>
            <li><a href=>General Education</a></li>
            <li><a href=>Field 7</a></li>
            <li><a href=>Field 8</a></li>
            <li><a href=>Field 9</a></li>
        </ul>
	</div>

	<div data-role="footer">
    	<!-- Navigation Bar at the bottom-->
		<div data-role="navbar">
			<ul>
                <li><a data-icon="grid" data-transition="pop" href="#browse">Browse</a></li>
                <li><a data-icon="search" data-transition="pop" href="#search">Search</a></li>
                <li><a data-icon="arrow-d" data-transition="pop" href="#lists">Lists</a></li>
			</ul>
		</div>
	</div>

</div><

<div data-role="page" id = "search" data-theme="a">
	    
    <div data-role="header">
        <div data-role="fieldcontain">
            <input type="search" name="password" id="search" value="" />
        </div>
    </div>
    
    <div data-role="content">
        <div data-role="fieldcontain">
            <label for="select-choice-1" class="select">Faculty</label>
            <select name="select-choice-1" id="select-choice-1">
                <option value="malan">David Malan</option>
                <option value="dowling">John Dowling</option>
                <option value="krakeur">Elissa Krakeur</option>
                <option value="macwilliam">Tommy MacWilliam</option>
            </select>     
            <a href="index.html" data-role="button" data-icon="search"  data-inline="true" data-theme="b">Search</a>
            <br/>      
            <label for="select-choice-1" class="select"> Day </label>
            <select name="select-choice-1" id="select-choice-1">
                <option value="mon">Mon</option>
                <option value="tue">Tue</option>
                <option value="wed">Wed</option>
                <option value="thurs">Thurs</option>
                <option value="fri">Fri</option>
            </select>
            <label for="mydate">Search by Time</label>
            <input name="mydate" id="mydate" type="date" data-role="datebox" data-options='{"mode": "timeflipbox"}'>
            <a href="index.html" data-role="button" data-icon="search"  data-inline="true" data-theme="b">Search</a>
        </div>
    </div>
    
    <div data-role="footer">
		<div data-role="navbar">
			<ul>
                <li><a data-icon="grid" data-transition="pop" href="#browse">Browse</a></li>
                <li><a data-icon="search" data-transition="pop" href="#search">Search</a></li>
                <li><a data-icon="arrow-d" data-transition="pop" href="#lists">Lists</a></li>
			</ul>
		</div>
	</div>
</div>

<div data-role="page" id = "lists" data-theme="a">

	<div data-role="header">
		<h1> Lists <h1>
	</div><!-- /header -->

	<div data-role="content">	
		<ul data-role="listview" data-theme="g">
            <li><a href=>Recently Viewed</a></li>
            <li><a href=>Courses  Taken</a></li>
            <li><a href=>Courses Shopped</a></li>
		</ul>
	</div>

	<div data-role="footer">
		<div data-role="navbar">
			<ul>
                <li><a data-icon="grid" data-transition="pop" href="#browse">Browse</a></li>
                <li><a data-icon="search" data-transition="pop" href="#search">Search</a></li>
                <li><a data-icon="arrow-d" data-transition="pop" href="#lists">Lists</a></li>
			</ul>
		</div>
	</div>

</div>
</body>
</html>
